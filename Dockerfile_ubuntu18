# Download base image ubuntu 18.04

FROM ubuntu:18.04

ARG NRP_USER=nrpuser
ARG NRP_GROUP=nrpgroup
ARG UID=1000
ARG GID=1000
ARG HOME_PARENT_FOLDER=/home

# Set environment

ENV HOME ${HOME_PARENT_FOLDER}/${NRP_USER}
ENV HOME_PARENT_FOLDER ${HOME_PARENT_FOLDER}
ENV NRP_INSTALL_DIR ${HOME}/.local

# Disable Prompt During Packages Installation

ARG DEBIAN_FRONTEND=noninteractive

# INSTALL sudo

RUN apt update -y && apt-get install -y sudo

ENV NVIDIA_DRIVER_CAPABILITIES compute,graphics,utility
ENV NVIDIA_VISIBLE_DEVICES=all

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends wget


# Installing some essential system packages
RUN apt-get update && apt-get install -y --no-install-recommends \
   lsb-release \
   build-essential \
   python3 python3-dev python3-pip \
   cmake \
   git \
   vim \
   ca-certificates \
   libzmqpp-dev \
   libopencv-dev \
   gnupg2 \
   && rm -rf /var/lib/apt/lists/*


# Set NRP_USER user

RUN mkdir -p ${HOME_PARENT_FOLDER} \
    && groupadd --gid ${GID} ${NRP_GROUP} \
    && useradd --home-dir ${HOME} --create-home --uid ${UID} --gid ${GID} --groups ${NRP_GROUP} -ms /bin/bash ${NRP_USER} \
    && echo "${NRP_USER} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# Set NRP_USER directories

RUN mkdir -p \
    ${NRP_INSTALL_DIR} \
    && chown -R ${NRP_USER}:${NRP_GROUP} $HOME



RUN /bin/bash -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' && \
    apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 

# Installing ROS  Melodic
RUN apt-get update && apt-get install -y --no-install-recommends \
   ros-melodic-desktop-full 

# Installing catkin tools
RUN apt-get update && apt-get install -y python3-setuptools && pip3 install catkin-tools

RUN apt-get install -y libgoogle-glog-dev protobuf-compiler ros-melodic-octomap-msgs ros-melodic-octomap-ros ros-melodic-joy python-vcstool

USER ${NRP_USER}
ENV USER ${NRP_USER}

# WORKDIR /home/${USER}

RUN echo "source /opt/ros/melodic/setup.bash" >> /home/${USER}/.bashrc



RUN mkdir -p ~/catkin_ws/src
WORKDIR /home/${USER}/catkin_ws/

RUN sudo apt-get install  -y python-pip
RUN sudo pip install catkin-tools

RUN /bin/bash -c '. /opt/ros/melodic/setup.bash; cd ~/catkin_ws/; catkin config --init --mkdirs --extend /opt/ros/$ROS_DISTRO --merge-devel --cmake-args -DCMAKE_BUILD_TYPE=Release'


WORKDIR /home/${USER}/catkin_ws/src

# RUN mkdir -p ~/catkin_ws/src/flightmare
COPY . /home/${USER}/catkin_ws/src/flightmare/
RUN sudo chown -R ${NRP_USER}:${NRP_GROUP} /home/${USER}/catkin_ws/src/flightmare/
# RUN git clone https://gitlab.com/erditum/flightmare.git \
#     && echo "export FLIGHTMARE_PATH=/home/flightmare" >>  /home/${USER}/.bashrc \
#     && .  /home/${USER}/.bashrc


RUN catkin build

# EOF
